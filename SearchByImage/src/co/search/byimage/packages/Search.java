package co.search.byimage.packages;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.TakesScreenshot;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;
import org.openqa.selenium.OutputType;

public class Search
{
	public static void main(String[] args)

	{

	
 WebDriver driver;
	
	
		try {

			System.setProperty("webdriver.chrome.driver","C:\\Users\\Priya.Gandhi\\Downloads\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			
			//Search by image in google
			
			driver.get("https://www.google.com/imghp?hl=EN");
			driver.findElement(By.className("S3Wjs")).click();
			Thread.sleep(1000);
			driver.findElement(By.name("image_url")).sendKeys("https://www.w3schools.com/howto/img_snow.jpg");
            driver.findElement(By.xpath("//input[@type='submit']")).click();
            Thread.sleep(1000);
            
            
           driver.findElement(By.className("iu-card-header")).click();
            
            //Validating search results

          
           String sKey = driver.findElement(By.xpath("//input[@type='text']")).getAttribute("value");
           System.out.println("Search string was =" + sKey);
            
            //Taking screenshot

	        java.io.File storefile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		    FileUtils.copyFile(storefile, new File("C:/Users/Priya.Gandhi/Documents/picture1.jpg"));
		   
          
		     } 

		catch (Exception e) 

		     {

			e.printStackTrace();

		     }
	}

	
		
			
}

